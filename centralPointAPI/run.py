# -*- coding: utf-8 -*-



""" 
Central Point.
http://www.codea.me/centralPoint 


(c) 2012 by Miguel Chavez Gamboa
miguel@codea.me 

License: BSD
"""


from eve import Eve

#for token generation.
import bcrypt

#for logging
import logging


#for Authentication
from eve.auth import TokenAuth
#for custome validation.
from bson import ObjectId
from eve.io.mongo import Validator


from flask import request

import datetime

def add_token(documents):
    for document in documents:
        print 'Creating token for %s'%document["username"]
        usr = document["username"]
        token = bcrypt.hashpw(usr, bcrypt.gensalt(10))
        document["token"] = token
        app.logger.debug('Creating token %s for user %s', token, document['username'])
        
class TokenAuth(TokenAuth):
    def check_auth(self, token,  resource, method):
        # use Eve's own db driver; no additional connections/resources are used
        accounts = app.data.driver.db['users']
        return accounts.find_one({'token': token})

if __name__ == '__main__':
    port = 5000
    host = '127.0.0.1'

    #app = Eve(auth=TokenAuth)
    app = Eve()

    #logger
    file_handler = logging.FileHandler('centralPointAPI.log')
    app.logger.addHandler(file_handler)
    app.logger.setLevel(logging.DEBUG)

    # Add a hook on users creation.
    app.on_insert_users += add_token


    app.run(host=host, port=port)
