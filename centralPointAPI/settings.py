# -*- coding: utf-8 -*-
"""
Central Point API
http://www.codea.me/centralPoint 


(c) 2012 by Miguel Chavez Gamboa
miguel@codea.me 

License: BSD
"""


MONGO_HOST = 'localhost'
MONGO_PORT = 27017
MONGO_USERNAME = 'centralpoint'
MONGO_PASSWORD = 'xarwit0721'
MONGO_DBNAME = 'centralpoint'

# let's not forget the API entry point
SERVER_NAME = '127.0.0.1:5000'




# Enable reads (GET), inserts (POST) and DELETE for resources/collections
# (if you omit this line, the API will default to ['GET'] and provide
# read-only access to the endpoint).
RESOURCE_METHODS = ['GET', 'POST']

# Enable reads (GET), edits (PATCH) and deletes of individual items
# (defaults to read-only item access).
ITEM_METHODS = ['GET']

# We enable standard client cache directives for all resources exposed by the
# API. We can always override these global settings later.
CACHE_CONTROL = 'max-age=20'
CACHE_EXPIRES = 20



users = {
    'additional_lookup': {
        'url': 'regex("[\w]+")', #'[\w]+', # This is for eve 0.2 or lower version
        'field': 'username'
    },
    
    # We disable endpoint caching as we don't want client apps to cache users data.
    'cache_control': '',
    'cache_expires': 0,

    # Allow 'token' to be returned with POST responses
    # NOTE: Really do this?... study more deeply this!.
    'extra_response_fields': ['token'],

    'schema': {
        'username': {
            'type': 'string',
            'minlength': 1,
            'maxlength': 50,
            'required': True,
            'unique': True,    #due to the fact that it is an entry point
        },
        'token': {
             'type': 'string',
             'required': True,
         },
    }
}


sensors = {
    'allow_unknown': True,
    'schema': {
        'loc_name':{
            'type': 'string',
            'required': True,
            'default': None,
        },
        'port_num': {
            'type': 'integer',
            'required': True,
            'unique': True,
            
        },
    }
}


alarms = {
    'schema': {
        'port_num': {
            'type': 'integer',
            'required': False,
            'unique': False,
            'data_relation': {
                'resource': 'sensors',
                'field': 'port_num',
                'embeddable': False
            },
        },
        'state':{
            'type': 'list',
            'allowed': ["ON", "OFF"],
            'required': True,
            'default': "OFF",
        },
        'datetime': {
            'type': 'datetime',
            'required': False,
            'checkDateConsistency':True
        },
    }
}



DOMAIN = {
    'users': users,
    'sensors': sensors,
    'alarms': alarms,
}


