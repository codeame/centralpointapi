# -*- coding: utf-8 -*-

"""
    eve-demo-client
    ~~~~~~~~~~~~~~~

    Simple and quickly hacked togheter, this script is used to reset the
    eve-demo API to its initial state. It will use standard API calls to:

        1) delete all items in the 'people' and 'works' collections
        2) post multiple items in both collection

    I guess it can also serve as a basic example of how to programmatically
    manage a remot e API using the phenomenal Requests library by Kenneth Reitz
    (a very basic 'get' function is included even if not used).

    :copyright: (c) 2012 by Nicola Iarocci.
    :license: BSD, see LICENSE for more details.
"""
import requests
import json
import random

import datetime
from bson import json_util

#ENTRY_POINT = 'http://eve-demo.herokuapp.com'
ENTRY_POINT = 'http://127.0.0.1:5000'


def post_people():
    users = [
        {
            'username': 'admin',
            'token': 'ASDF'

        },
        {
            'username': 'device',
            'token': 'QWERTY'
        },
    ]

    r = perform_post('users', json.dumps(users))
    print "'users' posted", r.status_code
    print 'R.text: %s\n'%r.text

    valids = []
    if r.status_code == 200:
        response = r.json()
        print response
        print '\n'
        for person in response:
            if person['_status'] == "OK":
                valids.append(person['_id'])

    return valids


def post_sensors(ids):
    dt = datetime.datetime.utcnow()
    sensors = [
        {
            'loc_name': 'Sensor1',
            'port_num': 1
        },
        {
            'loc_name': 'Sensor2',
            'port_num':2
        },
        {
            'loc_name': 'Sensor3',
            'port_num':3
        },
    ]

    r = perform_post('sensors', json.dumps(sensors))
    print "'' posted", r.status_code
    print 'JSON: %s\n'%r.text


def post_alarms(ids):
    dt = datetime.datetime.utcnow()
    alarms = [
        {
            'port_num':1,
            'state': ['OFF'],
            'datetime': dt.strftime("%a, %d %b %Y %H:%M:%S GMT")
        },
        {
            'port_num':2,
            'state': ['ON'],
            'datetime':dt.strftime("%a, %d %b %Y %H:%M:%S GMT")
        },
        {
            'port_num':3,
            'state': ['OFF'],
            'datetime':dt.strftime("%a, %d %b %Y %H:%M:%S GMT")
        },
    ]
    

    r = perform_post('alarms', json.dumps(alarms))
    print "'' posted", r.status_code
    print 'JSON: %s\n'%r.text


def perform_post(resource, data):
    headers = {'Content-Type': 'application/json'}
    return requests.post(endpoint(resource), data, headers=headers)


def delete():
    pass
    #print '...DELETING....'
    #r = perform_delete('users')
    #print "'users' deleted"
    #print r.status_code
    #r = perform_delete('clocks')
    #print "'clocks' deleted"
    #print r.status_code


def perform_delete(resource):
    return requests.delete(endpoint(resource))


def endpoint(resource):
    return '%s/%s/' % (ENTRY_POINT, resource)


def get():
    print '\n\nUSERS:\n'
    r = requests.get('http://127.0.0.1:5000/users')
    print r.text
    print '\n\n'
    print 'SENSORS:\n'
    r = requests.get('http://127.0.0.1:5000/sensors')
    print r.text
    print '\n\n'
    print 'ALARMS:\n'
    r = requests.get('http://127.0.0.1:5000/alarms')
    print r.text
    print '\n\n'

if __name__ == '__main__':
    delete()
    ids = post_people()
    post_sensors(ids)
    post_alarms(ids)
    get()